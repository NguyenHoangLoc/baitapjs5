//tính điểm chuẩn
const KV_A = "A";
const KV_B = "B";
const KV_C = "C";
const DT_1 = "1";
const DT_2 = "2";
const DT_3 = "3";
function tinhDiemKhuVuc(khuVuc) {
  if (khuVuc == KV_A) {
    return 2;
  } else if (khuVuc == KV_B) {
    return 1;
  } else if (khuVuc == KV_C) {
    return 0.5;
  } else {
    return 0;
  }
}
function tinhDiemDoiTuong(doiTuong) {
  if (doiTuong == DT_1) {
    return 2.5;
  } else if (doiTuong == DT_2) {
    return 1.5;
  } else if (doiTuong == DT_3) {
    return 1;
  } else {
    return 0;
  }
}
function tinhDiemChuan() {
  var score1 = document.getElementById("diemMon1").value * 1;
  console.log("score1: ", score1);
  var score2 = document.getElementById("diemMon2").value * 1;
  var score3 = document.getElementById("diemMon3").value * 1;
  var areaName = document.getElementById("arealist").value;
  var areaScore = tinhDiemKhuVuc(areaName);
  var areaSubject = document.getElementById("subjectlist");
  var subjectScore = tinhDiemDoiTuong(areaSubject);
  var sumScore = score1 + score2 + score3 + areaScore + subjectScore;
  var benchMark = document.getElementById("diemChuan").value * 1;
  console.log("benchMark: ", benchMark);

  if (score1 == 0 || score2 == 0 || score3 == 0) {
    document.getElementById("resultDiem").innerHTML =
      "bạn đã rớt do có môn 0 điểm";
  } else {
    if (sumScore < benchMark) {
      document.getElementById("resultDiem").innerHTML =
        "bạn đã rớt do không đủ điểm chuẩn";
    } else {
      document.getElementById("resultDiem").innerHTML = "Chúc mừng bạn đã đậu";
    }
  }
}
//tính tiền điện
const price1 = 500;
const price2 = 650;
const price3 = 850;
const price4 = 1100;
const price5 = 1300;

function tinhTienDien() {
  var nameCustomer = document.getElementById("nameCustomer").value;
  var numberKW = document.getElementById("numberKW").value * 1;
  var resultBill = 0;
  if (numberKW < 0) {
    resultBill = "Vui lòng nhập số KW lơn hơn 0";
  } else if (numberKW <= 50) {
    resultBill = numberKW * price1;
  } else if (numberKW <= 100) {
    resultBill = price1 * 50 + (numberKW - 50) * price2;
  } else if (numberKW <= 200) {
    resultBill = price1 * 50 + price2 * 50 + (numberKW - 100) * price3;
  } else if (numberKW <= 350) {
    resultBill =
      (price1 + price2) * 50 + price3 * 100 + (numberKW - 200) * price4;
  } else {
    resultBill =
      (price1 + price2) * 50 +
      price3 * 100 +
      price4 * 150 +
      (numberKW - 350) * price5;
  }
  document.getElementById(
    "resultBill"
  ).innerHTML = ` Họ Tên:${nameCustomer}, tiền điện: ${resultBill}`;
}
//tính thuế
function countmax60(thuNhap) {
  if (thuNhap > 60e6) {
    return 0;
  } else {
    return thuNhap * 0.05;
  }
}
function countmax120(thuNhap) {
  if (thuNhap > 60e6 && thuNhap <= 120e6) {
    return thuNhap * 0.1;
  } else {
    return 0;
  }
}
function countmax210(thuNhap) {
  if (thuNhap > 120e6 && thuNhap <= 210e6) {
    return thuNhap * 0.15;
  } else {
    return 0;
  }
}
function countmax384(thuNhap) {
  if (thuNhap > 210e6 && thuNhap <= 384e6) {
    return thuNhap * 0.2;
  } else {
    return 0;
  }
}
function countmax624(thuNhap) {
  if (thuNhap > 384e6 && thuNhap <= 624e6) {
    return thuNhap * 0.25;
  } else {
    return 0;
  }
}
function countmax960(thuNhap) {
  if (thuNhap > 624e6 && thuNhap <= 960e6) {
    return thuNhap * 0.3;
  } else {
    return 0;
  }
}
function countover960(thuNhap) {
  if (thuNhap > 960e6) {
    return thuNhap * 0.35;
  } else {
    return 0;
  }
}
function tinhThue() {
  var nameTax = document.getElementById("nameTax").value;
  var totalIncome = document.getElementById("totalIncome").value * 1;
  var numberDependent = document.getElementById("numberDependent").value * 1;
  var totalTaxIncome = totalIncome - 4e6 - numberDependent * 1.6e6;
  var totalTax =
    countmax60(totalTaxIncome) +
    countmax120(totalTaxIncome) +
    countmax210(totalTaxIncome) +
    countmax384(totalTaxIncome) +
    countmax624(totalTaxIncome) +
    countmax960(totalTaxIncome) +
    countover960(totalTaxIncome);
  if (totalIncome <= 0) {
    document.getElementById(
      "resultTax"
    ).innerHTML = ` Vui lòng nhập Tổng Thu nhập >0`;
  } else {
    document.getElementById(
      "resultTax"
    ).innerHTML = `Họ Tên: ${nameTax}; Tiền thuế thu nhập cá nhân:${totalTax} VND`;
  }
}
//Tính tiền cáp
const NhaDan = "ND";
const DoanhNghiep = "DN";
function disableNumberConnect() {
  var customList = document.getElementById("customList").value;
  document.getElementById("numberConnect").style.display =
    "DN" == customList ? "block" : "none";
}
function tinhPhiHD(loaiKH) {
  switch (loaiKH) {
    case NhaDan: {
      return 4.5;
    }
    case DoanhNghiep: {
      return 15;
    }
  }
}
function tinhPhiDV(loaiKH) {
  var numberConnect = document.getElementById("numberConnect").value * 1;
  switch (loaiKH) {
    case NhaDan: {
      return 20.5;
    }
    case DoanhNghiep: {
      if (numberConnect <= 10) {
        return 75;
      } else {
        return 75 + 5 * (numberConnect - 10);
      }
    }
  }
}
function tinhPhiThueKenh(loaiKH) {
  switch (loaiKH) {
    case NhaDan: {
      return 7.5;
    }
    case DoanhNghiep: {
      return 50;
    }
  }
}
function tinhTienCuoc() {
  var customerCode = document.getElementById("customerCode").value;
  var numberChannel = document.getElementById("numberChannel").value * 1;
  var customList = document.getElementById("customList").value;
  var phiHD = tinhPhiHD(customList);
  var phiDV = tinhPhiDV(customList);
  var phiThueKenh = tinhPhiThueKenh(customList) * numberChannel;
  var resultCableFee;
  resultCableFee = phiHD + phiDV + phiThueKenh;
  document.getElementById(
    "resultCableFee"
  ).innerHTML = `Mã Khách Hàng:${customerCode}. Tiền cáp: ${resultCableFee} $`;
}
